(alles Material gehört zur Publich Domain und kann daher von jedeR beliebig
    verwendet werden.
Kurzvortrag gehalten auf der Personalratsversammlung im hbz 14.12.2023
    )

# Kurzvortrag "Das Sabbatjahr"

Die Möglichkeiten der Beschäftigten zur Aufrechterhaltung der Gesundheit:
Das Sabbatjahr.


![Auf der Insel](insel.png)

(Aus https://bund-laender-nrw.verdi.de/service/thema-sabbaticals):

## Zweck
"Nicht für exotische Langzeiturlaube in der Südsee – das ist ein Mythos, der hin
und wieder von der Presse gepflegt wird, aber wenig mit der Realität zu tun hat.
In der Regel dienen Sabbaticals der Regeneration und
Neuorientierung, der beruflichen Weiterbildung oder werden für persönliche
Projekte oder Familienaufgaben genutzt."

## Durchführung
* Für Beamtinnen sowie Tarifbeschäftigte in Bund und Ländern relativ
einfach zu realisieren.
* Die Idee ist, ein paar Jahre etwas weniger zu verdienen bei gleicher
Arbeitszeit, um dann später für eine längere Zeit am Stück vom Dienst
freigestellt zu sein. Z.B. 7 Jahre lang auf 1/7 des Gehaltes zu verzichten bei
gleicher Arbeitszeit um danach ein Jahr frei zu haben.
* Es nuss nicht "ein Jahr" Sabbat genommen werden, und das Ansparmodell ist
flexibel, d.h. es kann sich über einen längeren oder kürzeren Zeitraum ziehen.

## Regeln in NRW

Ganz grob und auch wichtig:

### Beamte
Es dürfen dienstliche Belange nicht entgegenstehen.

### Tarifbeschäftigte
»Sabbatjahrmodelle« werden in § 6 Abs. 2 TV-L ausdrücklich erwähnt.

## Abschließend
Die Vereinbarung von Sabbaticalregelungen liegt im
Ermessen des "Dienstherrn". Er kann eine speziellen Arbeitszeitregelungen zur
Realisierung eines Sabbaticals ermöglichen, muss das aber nicht. Allerdings
ist der Dienstherr in seiner Entscheidungen nicht völlig frei. Er hat nach »pflichtgemäßem Ermessen« zu entscheiden.

